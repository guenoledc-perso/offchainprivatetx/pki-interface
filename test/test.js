var chai = require('chai');  
var assert = chai.assert;    // Using Assert style
var expect = chai.expect;    // Using Expect style


const async = require('async');
const fs = require('fs')
const NodeRsa = require('node-rsa')
const NaivePKI = require('../lib/naive-pki')

describe('should initialize the pki instance', () => {
    let pki
    let pkiPath = __dirname+'/pki'
    before(() => {
        pki = new NaivePKI(pkiPath)
    });
    it('should be initialized', () => {
        expect(pki).not.to.be.null
        expect(pki).to.have.property('getPublicKey')
        expect(pki).to.have.property('addPublicKey')
        expect(pki).to.have.property('whoIs')
        expect(fs.existsSync(pkiPath)).to.be.true
    });

    it('should add a public key', (done) => {
        const status=pki.addPublicKey('guenoledc', new NodeRsa({b:1024}).exportKey('pkcs1-public-pem'))
        expect(status).to.be.true
        const retrieved = pki.getPublicKey('guenoledc')
        expect(retrieved).not.to.be.null
        expect( new NodeRsa(retrieved, 'pkcs1-public-pem') ).not.to.throw
        done()
    });

    it('should find the identity of a given public key', (done) => {
        const pubKey = new NodeRsa({b:1024}).exportKey('pkcs1-public-pem')
        const status=pki.addPublicKey('guenoledc', pubKey)
        expect(status).to.be.true
        const identity = pki.whoIs(pubKey)
        expect(identity).to.equal('guenoledc')
        done()
    });

    it('should fail on border scenarios', () => {
        expect( pki.whoIs('not a public key') ).to.be.null
        expect( pki.whoIs(new NodeRsa({b:512}).exportKey('pkcs1-public-pem')) ).to.be.null
        expect( pki.addPublicKey('someone', 'not a public key') ).to.be.false 
        expect( pki.getPublicKey('nobody') ).to.be.null
        expect( ()=>pki.addPublicKey('', new NodeRsa({b:512}).exportKey('pkcs1-public-pem')) ).to.throw
        expect( ()=>pki.getPublicKey('') ).to.throw
    });
});