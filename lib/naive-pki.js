const fs = require('fs')
const NodeRsa = require('node-rsa')
const Hash = require('hash.js');

const mkdirp = require('mkdirp')
const AbstractPKI = require('./abstact-pki')

class NaivePKI extends AbstractPKI {
    constructor(path) {
        super()
        this._path = path || './.pki'
        mkdirp(this._path)
        mkdirp(this._path+'/ids')
    }

    addPublicKey(identity, publicKey) {
        if(!identity || typeof identity !=='string' || identity.length==0)
            throw new TypeError('identity must be a valid string')
        try {
            const key = new NodeRsa(publicKey, 'pkcs1-public-pem')
            const hash = Hash.sha1().update(key.exportKey('pkcs1-public-der')).digest('hex')
            
            fs.writeFileSync(this._path+'/'+identity+'.pub', key.exportKey('pkcs1-public-pem'))
            fs.writeFileSync(this._path+'/ids/'+hash, identity, 'utf8')
            return true
        } catch (failConvertingToRsaKey) {
            return false
        }
    }
    getPublicKey(identity) {
        if(!identity || typeof identity !=='string' || identity.length==0)
            throw new TypeError('identity must be a valid string')
        if(fs.existsSync(this._path+'/'+identity+'.pub')) {
            return fs.readFileSync(this._path+'/'+identity+'.pub','utf8')
        } else return null
    }
    whoIs(publicKey) {
        try {
            const key = new NodeRsa(publicKey, 'pkcs1-public-pem')
            const hash = Hash.sha1().update(key.exportKey('pkcs1-public-der')).digest('hex')
            
            if(fs.existsSync(this._path+'/ids/'+hash))
                return fs.readFileSync(this._path+'/ids/'+hash).toString('utf8')
            else return null
        } catch (failConvertingToRsaKey) {
            return null
        }
    }
}

module.exports=NaivePKI