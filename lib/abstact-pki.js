

class AbstractPKI {
    constructor() {
        if (this.constructor === AbstractPKI) throw new Error('Not implemented')
    }

    addPublicKey(identity, publicKey) {
        throw new Error('Not implemented')
    }

    getPublicKey(identity) {
        throw new Error('Unknown identity')
    }

    whoIs(publicKey) {
        throw new Error('Unknown public key')
    }

}

module.exports = AbstractPKI