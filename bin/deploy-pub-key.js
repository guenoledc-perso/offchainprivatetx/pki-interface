/**
 * Will take 3 parameters:
 *      - Folder where the pki is managed
 *      - name to give to the public key
 *      - public key file 
 */

const commandLineArgs = require('command-line-args')
const commandLineUsage = require('command-line-usage')

const optionDefinitions = [
    { name: 'path', type: String, defaultValue:'./pki', description: 'pki directory (default: ./pki)'},
    { name: 'name', type: String, description:'name of the public key' },
    { name: 'pubkey', type: String, description:'path to the PEM pub key file'},
    {
        name: 'help',
        alias: 'h',
        type: Boolean,
        description: 'Display this usage guide.'
      }
  ]
const options = commandLineArgs(optionDefinitions) 

if (options.help) {
  const usage = commandLineUsage([
    {
      header: 'Simple PKI tool',
      content: 'Used to deploy public keys into the pki folder structure'
    },
    {
      header: 'Options',
      optionList: optionDefinitions
    },
    {
      content: 'Project home: {underline https://gitlab.com/guenoledc-perso/offchainprivatetx}'
    }
  ])
  console.log(usage)
  process.exit(1)
} else {
  //console.log(options)
}

const fs = require('fs')
const PKI = require('../lib/naive-pki')

let pki = new PKI(options.path)

if(!options.name || !options.pubkey) {
    console.error('Provide the name and public key to be deployed (see --help)')
    process.exit(1)
}
if(!fs.existsSync(options.pubkey)) {
    console.error('The public key file cannot be found:' + options.pubkey)
    process.exit(1)
}
pki.addPublicKey(options.name, fs.readFileSync(options.pubkey))
console.log('Pub key deployed under name '+ options.name)